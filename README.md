## LOGC - Restaurant and Hotel LogBooks Application

Team 9's project for the end of Telerik Academy .NET Alpha 2019.

### Getting Started

To run the application, you can clone the repository and open it up from Visual Studio. Alternatively go to[ https://logcaspclient.azurewebsites.net](https://logcaspclient.azurewebsites.net)

### How the program works

After launch, roles are automatically created and an admin is created. Login credentials are admin@admin.com / Abc123! Customers, who are not logged, have access to all listed hotels and restaurants (aka establishments). They can leave reviews and rate the places. Managers have access to logbooks, assigned to them by the admin and their respective tasks. They can add tasks with tags and complete aforementioned tasks. Admins have access to the admin page where they create, delete, update establishments/logbooks/users/tags.

##### Main functionality of the application:

1. Create a new establishment;
2. Create a new logBook;
3. Create a new tag;
4. Create a new user;
5. Create a new customer comment;
6. Create a new manager task;
7. Delete establishment;
8. Delete user;
9. Delete logbook;
10. Delete tag;
11. Edit existing establishment;
12. Edit existing logBook;
13. Assign tags and manager to a logbook.

### Technologies

ASP.NET Core 2.2 MVC
Razor template engine
jQuery
Bootstrap
MS SQL Server
Entity Framework Core 2.2
ASP.NET Identity System
AJAX form communication
Continuous Integration - GitLab Pipelines
Microsoft Azure 

### Tools

Asana:

![alt_text](https://i.imgur.com/PzQeYdU.png "image_tooltip")

### Architecture

##### 1. Data layer:

*   Holds the models for the database

![alt_text](https://i.imgur.com/tep84vw.png "image_tooltip")

##### 2. Service layer:

*   Holds business logic
*   Returns database entities

##### 3. ASP layer:

![alt_text](https://i.imgur.com/GFBxF91.png "image_tooltip")

*   Holds the UI
*   Uses MVC
*   Calls to the Service Layer and receives entities in the controllers. 
*   Controllers user mappers to create ViewModels
*   ViewModels are injected in the Views

### User Design and Interaction (Phone)

![alt_text](https://i.imgur.com/It89xFp.png "image_tooltip")

### Authors

Anton Gervaziev
Selim Ahmedov

### License

Free to use

### Acknowledgments

We are grateful to the lecturers, mentors, colleagues for their support.