﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using RestaurantLogBook.ASPClient.Areas.Admin.Mappers;
using RestaurantLogBook.ASPClient.Areas.Manager.Mappers;
using RestaurantLogBook.ASPClient.Mappers;
using RestaurantLogBook.ASPClient.Services;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services;
using RestaurantLogBook.Services.Contracts;
using System.IO;

namespace RestaurantLogBook.ASPClient
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<ILogBookService, LogBookService>();
            services.AddScoped<IEstablishmentService, EstablishmentService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICustommerCommentService, CustommerCommentService>();
            services.AddScoped<IManagerTaskService, ManagerTaskService>();

            services.AddCustomMappers();
            services.AddCustomMappersCustomer();
            services.AddCustomMappersManager();
            services.AddRouting(options => options.LowercaseUrls = true);


            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));



            services.AddMvc()
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_2);

        }

        private void RegisterAuthentication(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                app.UseExceptionHandler("/Error/{0}");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                 name: "admin",
                 template: "{area:exists}/{controller=Users}/{action=AllUsers}/{id?}"
               );

                routes.MapRoute(
                 name: "manager",
                 template: "{area:exists}/{controller=ManagerLogBooks}/{action=Index}/{id?}"
               );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
