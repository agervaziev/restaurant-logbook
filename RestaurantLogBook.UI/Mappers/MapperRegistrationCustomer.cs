﻿using Microsoft.Extensions.DependencyInjection;
using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Mappers.Customer;
using RestaurantLogBook.ASPClient.Mappers.Home;
using RestaurantLogBook.ASPClient.Models;
using RestaurantLogBook.ASPClient.Models.CustomerViewModels;
using RestaurantLogBook.ASPClient.Models.HomeViewModels;
using RestaurantLogBook.Data.Models;
using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Mappers
{
    public static class MapperRegistrationCustomer
    {
        public static IServiceCollection AddCustomMappersCustomer(this IServiceCollection services)
        {
            services.AddSingleton<IViewModelMapper<Establishment, HomeEstablishmentViewModel>, HomeEstablishmentViewModelMapper>();
            services.AddSingleton<IViewModelMapper<ICollection<Establishment>, HomeViewModel>, HomeViewModelMapper>();

            services.AddSingleton<IViewModelMapper<Establishment, DetailsViewModel>, DetailsViewModelMapper>();
            services.AddSingleton<IViewModelMapper<CustomerComment, CustomerCommentViewModel>, CustomerCommentViewModelMapper>();
            return services;
        }
    }
}
