﻿using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Models;
using RestaurantLogBook.Data.Models;

namespace RestaurantLogBook.ASPClient.Mappers.Customer
{
    public class CustomerCommentViewModelMapper : IViewModelMapper<CustomerComment, CustomerCommentViewModel>
    {
        public CustomerCommentViewModel MapFrom(CustomerComment entity)
            => new CustomerCommentViewModel
            {
                CustomerCommentId = entity.CustomerCommentId,
                Date = entity.Date,
                Rating = entity.Rating,
                Text = entity.Text,
                Author = entity.Author
            };
    }
}
