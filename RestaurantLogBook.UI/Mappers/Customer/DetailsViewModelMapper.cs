﻿using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Models;
using RestaurantLogBook.ASPClient.Models.CustomerViewModels;
using RestaurantLogBook.Data.Models;
using System;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Mappers.Customer
{
    public class DetailsViewModelMapper : IViewModelMapper<Establishment, DetailsViewModel>
    {
        private readonly IViewModelMapper<CustomerComment, CustomerCommentViewModel> customerCommentMapper;

        public DetailsViewModelMapper(IViewModelMapper<CustomerComment, CustomerCommentViewModel> customerCommentMapper)
        {
            this.customerCommentMapper = customerCommentMapper ?? throw new ArgumentNullException(nameof(customerCommentMapper));
        }

        public DetailsViewModel MapFrom(Establishment entity)
            => new DetailsViewModel
            {
                Image = String.Format($"data:image/gif;base64,{Convert.ToBase64String(entity.Image)}"),
                Details = entity.Details,
                EstablishmentId = entity.EstablishmentId,
                EstablishmentName = entity.Name,
                Address = entity.Address,
                CustommerComments = entity.CustomerComments.Select(cc => this.customerCommentMapper.MapFrom(cc)).OrderByDescending(cc => cc.Date).ToList()
            };
    }
}
