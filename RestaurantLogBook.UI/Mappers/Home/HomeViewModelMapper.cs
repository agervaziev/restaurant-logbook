﻿using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Models.HomeViewModels;
using RestaurantLogBook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Mappers.Home
{
    public class HomeViewModelMapper : IViewModelMapper<ICollection<Establishment>, HomeViewModel>
    {
        private readonly IViewModelMapper<Establishment, HomeEstablishmentViewModel> establishmentMapper;

        public HomeViewModelMapper(IViewModelMapper<Establishment, HomeEstablishmentViewModel> establishmentMapper)
        {
            this.establishmentMapper = establishmentMapper ?? throw new ArgumentNullException(nameof(establishmentMapper));
        }

        public HomeViewModel MapFrom(ICollection<Establishment> entity)
        {
            return new HomeViewModel
            {
                Establishments = entity.Select(this.establishmentMapper.MapFrom).ToList()
            };
        }
    }
}
