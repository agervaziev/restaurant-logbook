﻿using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Models.HomeViewModels;
using RestaurantLogBook.Data.Models;
using System;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Mappers.Home
{
    public class HomeEstablishmentViewModelMapper : IViewModelMapper<Establishment, HomeEstablishmentViewModel>
    {
        public HomeEstablishmentViewModel MapFrom(Establishment entity)
        {

            var model = new HomeEstablishmentViewModel
            {
                Image = String.Format($"data:image/gif;base64,{Convert.ToBase64String(entity.Image)}"),
                EstablishmentId = entity.EstablishmentId,
                EstablishmentName = entity.Name,
                Address = entity.Address
            };
            if (entity.CustomerComments.Count == 0)
            {
                model.Rating = 0;
            }
            else
            {
                model.Rating = entity.CustomerComments.Select(cc => cc.Rating).Average();
            }
            return model;
        }
    }
}
