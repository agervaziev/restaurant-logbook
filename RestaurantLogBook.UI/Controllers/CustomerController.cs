﻿using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Models;
using RestaurantLogBook.ASPClient.Models.CustomerViewModels;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IEstablishmentService establishmentService;
        private readonly IViewModelMapper<Establishment, DetailsViewModel> detailsMapper;
        private readonly ICustommerCommentService customerCommentService;

        public CustomerController(IEstablishmentService establishmentService,
                                  IViewModelMapper<Establishment, DetailsViewModel> detailsMapper,
                                  ICustommerCommentService customerCommentService)
        {
            this.establishmentService = establishmentService ?? throw new ArgumentNullException(nameof(establishmentService));
            this.detailsMapper = detailsMapper ?? throw new ArgumentNullException(nameof(detailsMapper));
            this.customerCommentService = customerCommentService ?? throw new ArgumentNullException(nameof(customerCommentService));
        }


        public async Task<IActionResult> Details(int id)
        {
            var establishment = await this.establishmentService.GetEstablishmentByIdAsync(id);
            var model = this.detailsMapper.MapFrom(establishment);
            if (establishment.CustomerComments.Select(x => x.Rating).ToList().Count > 0)
            {
                model.Rating = establishment.CustomerComments.Select(x => x.Rating).Average();
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateComment(CustomerCommentViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var comment = await this.customerCommentService.AddCustomerCommentAsync(model.EstablishmentId, model.Author, model.Text, model.Rating);
                model.Author = comment.Author;
                model.Text = comment.Text;
                model.Date = comment.Date;
                return View("_CustomerCommentPartial", model);
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return BadRequest();
            }
        }
    }
}