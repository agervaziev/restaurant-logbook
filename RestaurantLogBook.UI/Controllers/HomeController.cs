﻿using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Models.HomeViewModels;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Controllers
{
    public class HomeController : Controller
    {

        private readonly IEstablishmentService establishmentService;
        private readonly IViewModelMapper<ICollection<Establishment>, HomeViewModel> establishmentMapper;

        public HomeController(IEstablishmentService establishmentService,
                              IViewModelMapper<ICollection<Establishment>, HomeViewModel> establishmentMapper)
        {
            this.establishmentService = establishmentService ?? throw new ArgumentNullException(nameof(establishmentService));
            this.establishmentMapper = establishmentMapper ?? throw new ArgumentNullException(nameof(establishmentMapper));
        }


        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("AllUsers", "Users", new { area = "Admin" });
            }
            if (User.IsInRole("Manager"))
            {
                return RedirectToAction("Index", "ManagerLogBooks", new { area = "Manager" });
            }
            var establishments = await this.establishmentService.GetAllEstablishmentsAsync(false, 8);
            var model = establishmentMapper.MapFrom(establishments);
            return View(model);
        }

        public async Task<IActionResult> ExpandEstablishments()
        {
            var establishments = await this.establishmentService.GetAllEstablishmentsAsync(true, 8);
            var model = establishmentMapper.MapFrom(establishments);
            return PartialView("_Establishments", model);
        }

        [HttpPost]
        public async Task<IActionResult> Search(string input)
        {
            try
            {
                if (string.IsNullOrEmpty(input))
                {
                    return Ok();
                }
                var establishments = await this.establishmentService.SearchEstablishment(input);
                var model = establishmentMapper.MapFrom(establishments);
                return PartialView("_Establishments", model);
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return BadRequest();
            }
        }
    }
}
