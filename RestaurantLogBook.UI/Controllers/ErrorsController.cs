﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace RestaurantLogBook.ASPClient.Controllers
{
    public class ErrorsController : Controller
    {
        [Route("Error/{statusCode}")]
        [AllowAnonymous]
        public IActionResult HandleErrorCode(int statusCode)
        {
            var statusCodeData = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            switch (statusCode)
            {
                case 404:
                    return View("~/Views/Errors/Error404.cshtml");
                case 500:
                    return View("~/Views/Errors/Error500.cshtml");
                default:
                    return View("~/Views/Errors/Default.cshtml");
            }
        }
    }
}