﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;

namespace RestaurantLogBook.ASPClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            SeedUsers(host);
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

        private static async void SeedUsers(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                if (await dbContext.Roles.AnyAsync(u => u.Name == "Admin"))
                {
                    return;
                }

                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                await roleManager.CreateAsync(new IdentityRole { Name = "Admin" });
                await roleManager.CreateAsync(new IdentityRole { Name = "Moderator" });
                await roleManager.CreateAsync(new IdentityRole { Name = "Manager" });

                var admin = new ApplicationUser { UserName = "admin@admin.com", Email = "admin@admin.com", Name = "Admin" };
                var result = await userManager.CreateAsync(admin, "Abc123!");
                await userManager.AddToRoleAsync(admin, "Admin");

            }
        }
    }
}
