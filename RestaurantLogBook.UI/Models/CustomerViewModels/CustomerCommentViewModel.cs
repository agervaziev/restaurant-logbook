﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Models
{
    public class CustomerCommentViewModel
    {
        public int EstablishmentId { get; set; }

        public int CustomerCommentId { get; set; }

        [Required]
        [Display(Name = "Your Rating")]
        public int Rating { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [StringLength(500, ErrorMessage = "The review must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [Display(Name = "Your Review")]
        public string Text { get; set; }

        [Display(Name = "Your Name")]
        public string Author { get; set; }
    }
}
