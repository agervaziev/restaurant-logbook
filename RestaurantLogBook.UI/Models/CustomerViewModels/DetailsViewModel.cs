﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.ASPClient.Models.CustomerViewModels
{
    public class DetailsViewModel
    {
        public int EstablishmentId { get; set; }
        public string EstablishmentName { get; set; }
        public string Image { get; set; }
        public double Rating { get; set; }
        public string Address { get; set; }
        public string Details { get; set; }
        public ICollection<CustomerCommentViewModel> CustommerComments { get; set; }

        [Display(Name = "Your Name")]
        public string AuthorName { get; set; }

        [Required]
        [StringLength(500, ErrorMessage = "The review must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [Display(Name = "Your Review")]
        public string ReviewText { get; set; }

        [Required]
        [Range(1, 5, ErrorMessage = "Please add rating")]
        [Display(Name = "Your Rating")]
        public int ReviewRating { get; set; }


    }
}
