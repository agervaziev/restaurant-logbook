﻿namespace RestaurantLogBook.ASPClient.Models.HomeViewModels
{
    public class HomeEstablishmentViewModel
    {
        public int EstablishmentId { get; set; }
        public string EstablishmentName { get; set; }
        public string Image { get; set; }
        public double Rating { get; set; }
        public string Address { get; set; }
    }
}
