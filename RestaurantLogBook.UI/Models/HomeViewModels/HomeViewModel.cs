﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Models.HomeViewModels
{
    public class HomeViewModel
    {
        public ICollection<HomeEstablishmentViewModel> Establishments { get; set; }
    }
}
