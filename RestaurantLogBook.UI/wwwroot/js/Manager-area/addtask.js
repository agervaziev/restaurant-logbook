﻿$(".final").on("click", function (ev) {
    ev.preventDefault();
    var $element = $(this);
    var logBookId = $element.data("id");
    var url = $element.data("ajaxurl");
    var tags = $element.parent().find("#tags").val();
    var text = $element.parent().find("#text").val();
    var author = $element.data("author");
    var assignee = $element.parent().find("#assignee").val();
    var antiForgery = ($('#anti-forgery-span').find('input'))[0];
    var model = {
        __RequestVerificationToken: antiForgery.value,
        LogBookId: logBookId,
        Author: author,
        Text: text,
        SelectedTags: tags,
        SelectedAssignee: assignee
    };
    //$element.parentsUntil('tbody').find('.add').hide();
    $.post(url, model)
        .done(function (responseData) {
            if (responseData.message.includes("Successfully")) {
                toastr.success(responseData.message);
                $element.closest('tr').fadeOut();
                $element.parentsUntil('#addform').fadeOut();
            }
            else {
                toastr.error(responseData.message);
            }
        })
})

$(".cancel").on('click', function () {
    $("#insert").find("#addform").remove();
    $(this).closest('tr').remove();

})