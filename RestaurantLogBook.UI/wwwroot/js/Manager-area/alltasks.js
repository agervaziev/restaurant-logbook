﻿$(document).ready(function () {
    $('#tasks').DataTable({
        "bLengthChange": false
    });
});

$(".done").on("click", function (ev) {
    ev.preventDefault();
    var $element = $(this);
    var status = $element.html();
    var parent = $element.parent();
    var id = $element.data("id");
    var url = "/Manager/ManagerLogBooks/ChangeTaskStatus/" + id
    $.get(url)
        .done(function (responseData) {
            if (status !== "Done") {
                $element.attr({
                    'class': "btn btn-success done"
                });
                $element.html("Done");
            }
            else {
                $element.attr({
                    'class': "btn btn-warning done"
                });
                $element.html("Not Done");
            }
        })
        .fail(function () {
            toastr.fail("Failed adding Task. Please try again later.")
        })
})

$(".add").on("click", function (ev) {
    ev.preventDefault();
    var $element = $(this);
    var id = $element.data("id");
    var url = $element.data("ajaxurl");
    $.get(url)
        .done(function (responseData) {
            $element.parent().append(responseData + '<br>');
        })
})