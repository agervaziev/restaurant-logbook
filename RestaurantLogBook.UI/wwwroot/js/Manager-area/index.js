﻿ $(".add").on("click", function (ev) {
            ev.preventDefault();
            var $element = $(this);
            var id = $element.data("id");
            var url = $element.data("ajaxurl");
            $.get(url)
                .done(function (responseData) {
                    $element.closest('tbody').prepend('<tr><td>' + responseData + '</td></tr>');
                    var targetOffset = $('#top').offset().top;
                    $('html, body').animate({ scrollTop: targetOffset }, 800);
                })
        })