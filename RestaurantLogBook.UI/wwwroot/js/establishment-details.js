﻿$(".demo").stars({ text: ["Awful", "Bad", "OK", "Good", "Perfect"] });

$("#review-form").on("submit", function (ev) {
    ev.preventDefault();
    $(this).validate();
    if ($(this).valid()) {
        var button = $(this).find("#submit");
        var parent = $(this).parent().parent();
        var id = $("#establishment-hidden").val();
        var author = $("#author").val();
        var rating = $("#rating-hidden").val();
        if (rating === 0) {
            $(this).find("#holder").empty();
            $(this).find("#holder").append('<p class="text-danger">Please enter rating</p>');
            return;
        }
        $(this).find("#holder").empty();
        button.prop("disabled", true);
        var text = $("#text").val();
        var url = "/Customer/CreateComment/";
        var model = {
            EstablishmentId: id,
            Author: author,
            Text: text,
            Rating: rating
        };
        $.post(url, model)
            .done(function (responseData) {
                toastr.success("Successfully added comment");
                parent.find("#author").val("");
                parent.find("#text").val("");
                button.prop("disabled", false);
                parent.find("#newreview").prepend(responseData);
                var hidable = parent.find("#noreview");
                hidable.hide();
                var targetOffset = $('#allreviews').offset().top;
                $('html, body').animate({ scrollTop: targetOffset }, 800);
            })
            .fail(function () {
                toastr.error("Something broke, please try again later");
            });
    }
});

$(document).ready(function () {
    $(this).find("#counter").nextAll().hide();
});


$(".text-box").profanityFilter({
    externalSwears: '/swearWords.json'
});

$("#counter").on("click", function () {
    $(this).nextAll().show();
    $(this).hide();
});
