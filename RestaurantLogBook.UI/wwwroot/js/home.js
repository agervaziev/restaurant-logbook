﻿$("#search").on("submit", function (ev) {
    ev.preventDefault();
    var button = $(this).find("#submit");
    var main = $(document).find("#main");
    var searchTerm = $(this).serialize();
    if ($(this).find("#term").val().trim() == "") {
        return;
    }
    button.prop("disabled", true);
    var url = "/Home/Search/";
    $.post(url, searchTerm)
        .done(function (responseData) {
            main.find("#establishments").empty();
            main.find("#establishments").append(responseData);
            button.prop("disabled", false);
            var targetOffset = $('#establishments').offset().top;
            $('html, body').animate({ scrollTop: targetOffset }, 1000);
        })
})

$("#expand").on("click", function (ev) {
    ev.preventDefault();
    var button = $(this);
    var main = $(document).find("#main");
    var url = "/Home/ExpandEstablishments/";
    $.get(url)
        .done(function (responseData) {
            main.find("#establishments").append(responseData);
            button.hide();
        })
})