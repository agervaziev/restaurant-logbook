﻿$("#deletehg").on("click", function (ev) {
    ev.preventDefault();
    var name = $(this).data("name");
    swal({
        title: "Are you sure?",
        text: "You will also delete all LogBooks for " + name + "!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                var $element = $(this);
                var id = $element.data('id');
                var redirect = $element.data('redirect');
                var url = $element.data('ajaxurl') + id;
                $.get(url)
                    .done(function (responseData) {
                        if (responseData.message.includes("Successfully")) {
                            window.location.href = redirect;
                        }
                        else {
                            toastr.error(responseData.message);
                        }
                    })
            }
            else {
                return false;
            }
        });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    readURL(this);
});