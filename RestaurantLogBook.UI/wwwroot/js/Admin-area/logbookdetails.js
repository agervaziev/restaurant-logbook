﻿ $(".delete").on("click", function (ev) {
            ev.preventDefault();
            var name = $(this).data("name");
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover " + name + "!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        var $element = $(this);
                var id = $element.data('id');
                var redirect = $element.data('redirect');
                var url = $element.data('ajaxurl') + id;
                $.get(url)
                    .done(function (responseData) {
                        if (responseData.message.includes("Successfully")) {
                            toastr.success(responseData.message + "@Model.LogBookName");
                            window.location.href = redirect;
                        }
                        else {
                            toastr.error(responseData.message);
                        }
                    })
                    }
                    else {
                        return false;
                    }
                });
        })
