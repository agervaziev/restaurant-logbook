﻿$("#create").on("click", function (ev) {
    ev.preventDefault();
    var $element = $(this);
    var table = $("#example").DataTable;
    console.log(table);
    var url = $element.data("ajaxurl");
    var field = $element.parent().find("#name");
    var name = $element.parent().find("#name").val();
    var antiForgery = ($('#anti-forgery-span').find('input'))[0];
    var model = {
        __RequestVerificationToken: antiForgery.value,
        TagName: name
    };
    $.post(url, model)
        .done(function (responseData) {
            if (responseData.message.includes("Successfully")) {
                toastr.success(responseData.message);
                field.val("");
            }
            else {
                toastr.error(responseData.message);
            }
        })
})


$(".delete").on("click", function (ev) {
    ev.preventDefault();
    var name = $(this).data("name");
    swal({
        title: "Are you sure you want to delete " + name + "?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                var $element = $(this);
                var url = $element.data('ajaxurl');
                var message = $element.data('successMessage')
                $.get(url)
                    .done(function (responseData) {
                        $element.closest('tr').remove();
                        toastr.success(message);
                    })
            }
            else {
                return false;
            }
        });
});