﻿$(".delete").on("click", function (ev) {
    ev.preventDefault();
    var user = $(this).data('name');
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover " + user + "!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                var $element = $(this);
                var url = $element.data('ajaxurl');
                $.get(url)
                    .done(function (responseData) {
                        $element.closest('tr').remove();
                        toastr.success(responseData.message);
                    })
                    .fail(function (responseData) {
                        toastr.fail("Unable to delete user. Please try again later");
                    })
            }
            else {
                swal(user + " is safe!");
            }
        });
})