﻿$("#est-form").on("submit", function (ev) {
    ev.preventDefault();
    var $element = $(this);
    var url = $element.data('ajaxurl');
    var antiForgery = ($('#anti-forgery-span').find('input'))[0];
    var $this = $(this);
    var redirect = "/admin/establishments/allestablishments";
    var formData = new FormData(this);
    formData.__RequestVerificationToken = antiForgery.value;
    $.ajax({
        type: 'POST',
        url: url,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.message.includes("Successfully")) {
                toastr.success(data.message);
                window.location.href = redirect;
            }
            else {
                toastr.error(data.message);
            }
        }
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    readURL(this);
});