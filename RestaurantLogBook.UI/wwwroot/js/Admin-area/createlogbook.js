﻿$(".create").on("click", function (ev) {
    ev.preventDefault();
    var $element = $(this);
    var url = "/Admin/LogBooks/Create/";
    var redirect = "/admin/logbooks/alllogbooks";
    var name = $element.parent().find("#name").val();
    var establishment = $element.parent().find("#establishment").val();
    var antiForgery = ($('#anti-forgery-span').find('input'))[0];
    var tags = $element.parent().find("#tags").val();
    var managers = $element.parent().find("#managers").val();
    var model = {
        __RequestVerificationToken: antiForgery.value,
        LogBookName: name,
        SelectedEstablishment: establishment,
        SelectedTags: tags,
        SelectedManagers: managers
    };
    $.post(url, model)
        .done(function (responseData) {
            if (responseData.message.includes("Successfully")) {
                toastr.success(responseData.message);
                window.location.href = redirect;
            }
            else {
                toastr.error(responseData.message);
            }
        })
})