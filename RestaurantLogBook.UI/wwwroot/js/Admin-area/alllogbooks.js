﻿$(".tags-select").each(function () {
    var currentSelect = $(this);

    var logbookId = currentSelect.data('logbookId');
    var url = "/Admin/LogBooks/GetTagsForLogbook/" + logbookId;
    currentSelect.select2({
        placeholder: 'Select tags',
        allowclear: true,
        theme: "classic",
        ajax: {
            url: url,
            datatype: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.map(x => {
                        return { id: x.tagId, text: x.name }
                    })
                };
            }
        },
    })
    currentSelect.on('change', function () {
        var textBoxValueData = $(this).val();
        var url2 = '/Admin/LogBooks/SaveTags/' + logbookId + '?data=' + textBoxValueData;
        $.ajax({
            url: url2,
            dataType: 'json',
            type: 'post',
        });
    })
});