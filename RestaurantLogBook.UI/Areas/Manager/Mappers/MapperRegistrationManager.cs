﻿using Microsoft.Extensions.DependencyInjection;
using RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Manager.Models;
using RestaurantLogBook.Data.Models;
using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Mappers
{
    public static class MapperRegistrationManager
    {
        public static IServiceCollection AddCustomMappersManager(this IServiceCollection services)
        {
            services.AddSingleton<IManagerViewModelMapper<LogBook, ManagerLogBookViewModel>, ManagerLogBookViewModelMapper>();
            services.AddSingleton<IManagerViewModelMapper<ICollection<LogBook>, AllManagerLogBooksViewModel>, AllManagerLogBooksViewModelMapper>();

            services.AddSingleton<IManagerViewModelMapper<ManagerTask, TaskViewModel>, TaskViewModelMapper>();
            services.AddSingleton<IManagerViewModelMapper<ICollection<ManagerTask>, AllTasksViewModel>, AllTasksViewModelMapper>();
            return services;
        }
    }
}
