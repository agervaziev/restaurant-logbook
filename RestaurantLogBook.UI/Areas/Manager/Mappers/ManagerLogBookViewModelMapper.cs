﻿using RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Manager.Models;
using RestaurantLogBook.Data.Models;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Mappers
{
    public class ManagerLogBookViewModelMapper : IManagerViewModelMapper<LogBook, ManagerLogBookViewModel>
    {
        public ManagerLogBookViewModel MapFrom(LogBook entity)
            => new ManagerLogBookViewModel
            {
                LogBookId = entity.LogBookId,
                LogBookName = entity.Name,
                Tags = entity.LogBookTags.Select(t => t.Tag.Name).ToList(),
                PossibleAssignees = entity.LogBookManagers.Select(lbm => lbm.Manager.Name).ToList()
            };
    }
}
