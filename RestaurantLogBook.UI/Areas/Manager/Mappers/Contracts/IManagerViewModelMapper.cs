﻿namespace RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts
{
    public interface IManagerViewModelMapper<TEntity, TViewModel>
    {
        TViewModel MapFrom(TEntity entity);
    }
}
