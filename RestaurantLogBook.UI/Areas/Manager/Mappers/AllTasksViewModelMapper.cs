﻿using RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Manager.Models;
using RestaurantLogBook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Mappers
{
    public class AllTasksViewModelMapper : IManagerViewModelMapper<ICollection<ManagerTask>, AllTasksViewModel>
    {
        private readonly IManagerViewModelMapper<ManagerTask, TaskViewModel> taskMapper;

        public AllTasksViewModelMapper(IManagerViewModelMapper<ManagerTask, TaskViewModel> taskMapper)
        {
            this.taskMapper = taskMapper ?? throw new ArgumentNullException(nameof(taskMapper));
        }

        public AllTasksViewModel MapFrom(ICollection<ManagerTask> entity)
        {
            return new AllTasksViewModel
            {
                LogBookName = entity.First().LogBook.Name,
                LogBookId = entity.First().LogBookId,
                Tasks = entity.Select(this.taskMapper.MapFrom).ToList(),
                PossibleAssignees = entity.First().LogBook.LogBookManagers.Select(lbm => lbm.Manager.Name).ToList(),
                PossibleTags = entity.First().LogBook.LogBookTags.Select(t => t.Tag.Name).ToList()
        };
        }
    }
}
