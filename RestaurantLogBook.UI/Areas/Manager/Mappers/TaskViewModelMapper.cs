﻿using RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Manager.Models;
using RestaurantLogBook.Data.Models;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Mappers
{
    public class TaskViewModelMapper : IManagerViewModelMapper<ManagerTask, TaskViewModel>
    {
        public TaskViewModel MapFrom(ManagerTask entity)
        {
            var model = new TaskViewModel
            {
                TaskId = entity.ManagerCommentId,
                Author = entity.Author.Name,
                Text = entity.Text,
                Date = entity.Date,
                IsDone = entity.IsDone,
                Tags = entity.ManagerCommentTags.Select(mct => mct.Tag.Name).ToList()
            };
            if(entity.Assignee != null)
            {
                model.Assignee = entity.Assignee.Name;
            }
            else
            {
                model.Assignee = "Unassigned";
            }
            return model;
        }
    }
}
