﻿using RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Manager.Models;
using RestaurantLogBook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Mappers
{
    public class AllManagerLogBooksViewModelMapper : IManagerViewModelMapper<ICollection<LogBook>, AllManagerLogBooksViewModel>
    {

        private readonly IManagerViewModelMapper<LogBook, ManagerLogBookViewModel> logBookMapper;

        public AllManagerLogBooksViewModelMapper(IManagerViewModelMapper<LogBook, ManagerLogBookViewModel> logBookMapper)
        {
            this.logBookMapper = logBookMapper ?? throw new ArgumentNullException(nameof(logBookMapper));
        }

        public AllManagerLogBooksViewModel MapFrom(ICollection<LogBook> entity)
        {
            return new AllManagerLogBooksViewModel
            {
                LogBooks = entity.Select(this.logBookMapper.MapFrom).ToList()
            };
        }
    }
}
