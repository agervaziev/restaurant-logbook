﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Models
{
    public class ManagerLogBookViewModel
    {
        public int LogBookId { get; set; }
        public string LogBookName { get; set; }
        public ICollection<string> Tags { get; set; }
        public string Author { get; set; }
        public ICollection<string> PossibleAssignees { get; set; }
    }
}
