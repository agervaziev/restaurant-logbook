﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Models
{
    public class AllTasksViewModel
    {
        public string LogBookName { get; set; }
        public int LogBookId { get; set; }
        public ICollection<TaskViewModel> Tasks { get; set; }
        public ICollection<string> PossibleAssignees { get; set; }
        public ICollection<string> PossibleTags { get; set; }
    }
}
