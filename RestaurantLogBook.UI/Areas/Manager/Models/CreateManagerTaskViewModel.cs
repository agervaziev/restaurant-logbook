﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Models
{
    public class CreateManagerTaskViewModel
    {
        public string LogBookName { get; set; }
        public int LogBookId { get; set; }
        public string Author { get; set; }
        public ICollection<string> PossibleAssignees { get; set; }
        [Required]
        public string Text { get; set; }
        public ICollection<string> AllTags { get; set; }
        public ICollection<string> SelectedTags { get; set; }
        public string SelectedAssignee { get; set; }
    }
}
