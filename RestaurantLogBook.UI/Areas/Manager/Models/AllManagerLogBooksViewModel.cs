﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Models
{
    public class AllManagerLogBooksViewModel
    {
       public ICollection<ManagerLogBookViewModel> LogBooks { get; set; }
    }
}
