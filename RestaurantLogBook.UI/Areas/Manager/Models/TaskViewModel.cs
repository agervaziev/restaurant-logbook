﻿using System;
using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Models
{
    public class TaskViewModel
    {
        public int TaskId { get; set; }
        public string Author { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public bool IsDone { get; set; }
        public string Assignee { get; set; }
        public ICollection<string> Tags { get; set; }
    }
}
