﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Areas.Manager.Models
{
    public class FilterViewModel
    {
        public int LogBookId { get; set; }
        public string AuthorFilter { get; set; }
        public string DateFilter { get; set; }
        public bool IsDoneFilter { get; set; }
        public string AssigneeFilter { get; set; }
        public ICollection<string> TagsFilter { get; set; }

        //public ICollection<TaskViewModel> Tasks { get; set; }
    }
}
