﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Areas.Manager.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Manager.Models;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace RestaurantLogBook.ASPClient.Areas.Manager.Controllers
{
    [Area("Manager")]
    [Authorize(Roles = "Manager")]
    public class ManagerLogBooksController : Controller
    {

        private readonly ILogBookService logBookService;
        private readonly IManagerViewModelMapper<ICollection<LogBook>, AllManagerLogBooksViewModel> logBooksMapper;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IManagerTaskService managerTaskService;
        private readonly IManagerViewModelMapper<ICollection<ManagerTask>, AllTasksViewModel> tasksMapper;

        public ManagerLogBooksController(ILogBookService logBookService, IManagerViewModelMapper<ICollection<LogBook>, AllManagerLogBooksViewModel> logBooksMapper, UserManager<ApplicationUser> userManager, IManagerTaskService managerTaskService, IManagerViewModelMapper<ICollection<ManagerTask>, AllTasksViewModel> tasksMapper)
        {
            this.logBookService = logBookService ?? throw new ArgumentNullException(nameof(logBookService));
            this.logBooksMapper = logBooksMapper ?? throw new ArgumentNullException(nameof(logBooksMapper));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            this.managerTaskService = managerTaskService ?? throw new ArgumentNullException(nameof(managerTaskService));
            this.tasksMapper = tasksMapper ?? throw new ArgumentNullException(nameof(tasksMapper));
        }

        public async Task<IActionResult> Index()
        {
            var userName = User.Identity.Name;
            var logbooks = await this.logBookService.GetAllLogBooksAsync(userName);
            var model = this.logBooksMapper.MapFrom(logbooks);
            return View(model);
        }

        public async Task<IActionResult> AddTask(int id)
        {
            var logBook = await this.logBookService.GetLogBookBy(id);
            var model = new CreateManagerTaskViewModel()
            {
                LogBookId = id,
                AllTags = logBook.LogBookTags.Select(t => t.Tag.Name).ToList(),
                Author = User.Identity.Name,
                LogBookName = logBook.Name
            };
            return PartialView("_AddTaskPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTask(CreateManagerTaskViewModel model)
        {
            var logBook = await this.logBookService.GetLogBookBy(model.LogBookId);
            if (!this.ModelState.IsValid)
            {
                model.AllTags = logBook.LogBookTags.Select(t => t.Tag.Name).ToList();
                model.Author = User.Identity.Name;
                model.LogBookName = logBook.Name;
                model.PossibleAssignees = logBook.LogBookManagers.Select(lbm => lbm.Manager.Name).ToList();
                return View(model);
            }
            try
            {
                await this.managerTaskService.AddManagerTaskAsync(model.Author, model.LogBookId, model.SelectedTags, model.Text, model.SelectedAssignee);
                return Json(new { message = "Successfully added task" });
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }
        }

        public async Task<IActionResult> AllTasks(int id)
        {
            var logBook = await this.logBookService.GetLogBookBy(id);
            if (logBook.ManagerTasks.Count == 0)
            {
                return View(new AllTasksViewModel(){ LogBookId = logBook.LogBookId, LogBookName = logBook.Name, Tasks = null });
            }
            var model = this.tasksMapper.MapFrom(logBook.ManagerTasks);
            return View(model);
        }

        public async Task<IActionResult> ChangeTaskStatus(int id)
        {
            try
            {
                var task = await this.managerTaskService.ChangeTaskStatusAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<IActionResult> FilterTasksStatus(FilterViewModel model)
        {
            try
            {
                //var task = await this.managerTaskService.Filter(model);
                return Ok();
            }
            catch
            {
                return Ok();
            }
        }

    }
}