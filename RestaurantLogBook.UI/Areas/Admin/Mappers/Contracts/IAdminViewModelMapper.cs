﻿namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts
{
    public interface IAdminViewModelMapper<TEntity, TViewModel>
    {
        TViewModel MapFrom(TEntity entity);
    }
}
