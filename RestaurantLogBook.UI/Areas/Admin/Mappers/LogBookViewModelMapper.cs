﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class LogBookViewModelMapper : IAdminViewModelMapper<LogBook, LogBookViewModel>
    {

        public LogBookViewModel MapFrom(LogBook entity)
            => new LogBookViewModel
            {
                LogBookName = entity.Name,
                Managers = entity.LogBookManagers.Select(lbm => lbm.Manager.Name).ToList(),
                Tags = entity.LogBookTags.Select(lbt => lbt.Tag.Name).ToList(),
                EstablishmentName = entity.Establishment.Name,
                LogBookId = entity.LogBookId

            };
    }
}
