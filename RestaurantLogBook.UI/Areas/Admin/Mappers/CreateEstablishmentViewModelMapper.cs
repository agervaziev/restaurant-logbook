﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class CreateEstablishmentViewModelMapper : IAdminViewModelMapper<Establishment, CreateEstablishmentViewModel>
    {
        public CreateEstablishmentViewModel MapFrom(Establishment entity)
            => new CreateEstablishmentViewModel
            {
                Details = entity.Details,
                EstablishmentAddress = entity.Address,
                EstablishmentId = entity.EstablishmentId,
                EstablishmentName = entity.Name,
                DisplayImage = String.Format($"data:image/gif;base64,{Convert.ToBase64String(entity.Image)}")
            };
    }
}
