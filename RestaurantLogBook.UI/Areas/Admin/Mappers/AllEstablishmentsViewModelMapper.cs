﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class AllEstablishmentsViewModelMapper : IAdminViewModelMapper<ICollection<Establishment>, AllEstablishmentsViewModel>
    {

        private readonly IAdminViewModelMapper<Establishment, EstablishmentViewModel> establishmentMapper;

        public AllEstablishmentsViewModelMapper(IAdminViewModelMapper<Establishment, EstablishmentViewModel> establishmentMapper)
        {
            this.establishmentMapper = establishmentMapper ?? throw new ArgumentNullException(nameof(establishmentMapper));
        }

        public AllEstablishmentsViewModel MapFrom(ICollection<Establishment> entity)
        {
            return new AllEstablishmentsViewModel
            {
                Establishments = entity.Select(this.establishmentMapper.MapFrom).ToList()
            };
        }
    }
}
