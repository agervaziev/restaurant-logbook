﻿using Microsoft.Extensions.DependencyInjection;
using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public static class MapperRegistrationManager
    {
        public static IServiceCollection AddCustomMappers(this IServiceCollection services)
        {
            services.AddSingleton<IAdminViewModelMapper<Tag, TagViewModel>, TagViewModelMapper>();
            services.AddSingleton<IAdminViewModelMapper<ICollection<Tag>, AllTagsViewModel>, AllTagsViewModelMapper>();

            services.AddSingleton<IAdminViewModelMapper<Establishment, CreateEstablishmentViewModel>, CreateEstablishmentViewModelMapper>();
            services.AddSingleton<IAdminViewModelMapper<Establishment, EstablishmentDetailsViewModel>, EstablishmentDetailsViewModelMapper>();
            services.AddSingleton<IAdminViewModelMapper<Establishment, EstablishmentViewModel>, EstablishmentViewModelMapper>();
            services.AddSingleton<IAdminViewModelMapper<ICollection<Establishment>, AllEstablishmentsViewModel>, AllEstablishmentsViewModelMapper>();

            services.AddSingleton<IAdminViewModelMapper<LogBook, LogBookViewModel>, LogBookViewModelMapper>();
            services.AddSingleton<IAdminViewModelMapper<ICollection<LogBook>, AllLogBooksViewModel>, AllLogBooksViewModelMapper>();

            services.AddSingleton<IAdminViewModelMapper<ApplicationUser, UserViewModel>, UserViewModelMapper>();
            services.AddSingleton<IAdminViewModelMapper<ICollection<ApplicationUser>, AllUsersViewModel>, AllUsersViewModelMapper>();

            return services;
        }
    }
}
