﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class UserViewModelMapper : IAdminViewModelMapper<ApplicationUser, UserViewModel>
    {
        public UserViewModel MapFrom(ApplicationUser entity)
            => new UserViewModel
            {
                UserId = entity.Id,
                Email = entity.Email,
                Name = entity.Name
            };
    }
}
