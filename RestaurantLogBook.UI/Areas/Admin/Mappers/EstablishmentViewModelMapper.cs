﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class EstablishmentViewModelMapper : IAdminViewModelMapper<Establishment, EstablishmentViewModel>
    {
        public EstablishmentViewModel MapFrom(Establishment entity)
            => new EstablishmentViewModel
            {
                EstablishmentId = entity.EstablishmentId,
                EstablishmentName = entity.Name,
                LogBookNames = entity.LogBooks.Select(lb => lb.Name).ToList(),
                Details = entity.Details
            };
    }
}
