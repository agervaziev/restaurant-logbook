﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class AllTagsViewModelMapper : IAdminViewModelMapper<ICollection<Tag>, AllTagsViewModel>
    {
        private readonly IAdminViewModelMapper<Tag, TagViewModel> tagMapper;

        public AllTagsViewModelMapper(IAdminViewModelMapper<Tag, TagViewModel> tagMapper)
        {
            this.tagMapper = tagMapper ?? throw new System.ArgumentNullException(nameof(tagMapper));
        }

        public AllTagsViewModel MapFrom(ICollection<Tag> entity)
        {
            return new AllTagsViewModel
            {
                Tags = entity.Select(this.tagMapper.MapFrom).ToList()
            };
        }
    }
}
