﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class TagViewModelMapper : IAdminViewModelMapper<Tag, TagViewModel>
    {
        public TagViewModel MapFrom(Tag entity)
            => new TagViewModel
            {
                TagId = entity.TagId,
                TagName = entity.Name
            };
    }
}
