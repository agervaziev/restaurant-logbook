﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class AllUsersViewModelMapper : IAdminViewModelMapper<ICollection<ApplicationUser>, AllUsersViewModel>
    {
        private readonly IAdminViewModelMapper<ApplicationUser, UserViewModel> userMapper;

        public AllUsersViewModelMapper(IAdminViewModelMapper<ApplicationUser, UserViewModel> userMapper)
        {
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
        }

        public AllUsersViewModel MapFrom(ICollection<ApplicationUser> entity)
        {
            return new AllUsersViewModel
            {
                Users = entity.Select(this.userMapper.MapFrom).ToList()
            };
        }
    }
}
