﻿using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Mappers
{
    public class AllLogBooksViewModelMapper : IAdminViewModelMapper<ICollection<LogBook>, AllLogBooksViewModel>
    {

        private readonly IAdminViewModelMapper<LogBook, LogBookViewModel> logBookMapper;

        public AllLogBooksViewModelMapper(IAdminViewModelMapper<LogBook, LogBookViewModel> logBookMapper)
        {
            this.logBookMapper = logBookMapper ?? throw new ArgumentNullException(nameof(logBookMapper));
        }

        public AllLogBooksViewModel MapFrom(ICollection<LogBook> entity)
        {
            return new AllLogBooksViewModel
            {
                LogBooks = entity.Select(this.logBookMapper.MapFrom).ToList()
            };
        }
    }
}
