﻿using Microsoft.AspNetCore.Http;
using RestaurantLogBook.ASPClient.Areas.Admin.CustomValidations;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class EstablishmentDetailsViewModel
    {
        public int EstablishmentId { get; set; }

        [Required(ErrorMessage = "Please enter a name.")]
        [StringLength(30, ErrorMessage = "Name should be between 3 and 30 symbols. ", MinimumLength = 3)]
        [Display(Name = "Establishment Name")]
        public string EstablishmentName { get; set; }

        [ValidateFile(ErrorMessage = "Please select a png/gpeg image smaller than 1MB")]
        public IFormFile Image { get; set; }

        public string DisplayImage { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string EstablishmentAddress { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "Name should be between 5 and 1000 symbols. ", MinimumLength = 5)]
        public string Details { get; set; }
    }
}
