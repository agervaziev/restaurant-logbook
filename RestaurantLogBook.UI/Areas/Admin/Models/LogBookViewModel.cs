﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class LogBookViewModel
    {
        public int LogBookId { get; set; }
        public string LogBookName { get; set; }
        public string EstablishmentName { get; set; }
        public ICollection<string> Managers { get; set; }
        public ICollection<string> Tags { get; set; }
    }
}
