﻿namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class UserViewModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool IsLogged { get; set; }
        public string Email { get; set; }
    }
}
