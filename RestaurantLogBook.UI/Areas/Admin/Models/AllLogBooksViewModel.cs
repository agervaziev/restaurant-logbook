﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class AllLogBooksViewModel
    {
        public ICollection<LogBookViewModel> LogBooks { get; set; }
    }
}
