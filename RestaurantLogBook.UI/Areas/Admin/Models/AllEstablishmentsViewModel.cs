﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class AllEstablishmentsViewModel
    {
       public ICollection<EstablishmentViewModel> Establishments { get; set; }
    }
}
