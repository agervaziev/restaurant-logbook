﻿namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class TagViewModel
    {
        public int TagId { get; set; }

        public string TagName { get; set; }
    }
}
