﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class AllUsersViewModel
    {
        public ICollection<UserViewModel> Users { get; set; }
    }
}
