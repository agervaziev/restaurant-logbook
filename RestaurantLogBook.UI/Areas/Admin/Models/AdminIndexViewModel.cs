﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class AdminIndexViewModel
    {
        public Establishment Establishment { get; set; }

        public ICollection<ApplicationUser> Managers { get; set; }

        public ICollection<Tag> Tags { get; set; }
    }
}
