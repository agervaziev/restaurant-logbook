﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class CreateLogBookViewModel
    {
        public int LogBookId { get; set; }

        public ICollection<string> AllEstablishments { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Name should be less than 50 symbols", MinimumLength = 1)]
        [Display(Name = "Enter LogBook name")]
        public string LogBookName { get; set; }

        [Display(Name = "Select establishment")]
        [Required]
        public string SelectedEstablishment { get; set; }

        public ICollection<string> AllTags { get; set; }

        [Display(Name = "Select tag/s")]
        public ICollection<string> SelectedTags {get; set;}

        public ICollection<string> AllManagerNames { get; set; }

        [Display(Name = "Select manager/s")]
        public ICollection<string> SelectedManagers { get; set; }
    }
}
