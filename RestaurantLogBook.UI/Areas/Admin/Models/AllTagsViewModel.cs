﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class AllTagsViewModel
    {
        public ICollection<TagViewModel> Tags { get; set; }

        [Required]
        [MaxLength(15)]
        public string TagName { get; set; }
    }
}
