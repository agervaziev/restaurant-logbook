﻿using System.Collections.Generic;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Models
{
    public class EstablishmentViewModel
    {
        public int EstablishmentId { get; set; }
        public string EstablishmentName { get; set; }
        public ICollection<string> LogBookNames { get; set; }
        public string Details { get; set; }
    }
}
