﻿using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Controllers
{
    public class UsersController : AdminControllerBase
    {

        private readonly IUserService userService;
        private readonly IAdminViewModelMapper<ICollection<ApplicationUser>, AllUsersViewModel> usersMapper;

        public UsersController(IUserService userService,
                               IAdminViewModelMapper<ICollection<ApplicationUser>, AllUsersViewModel> usersMapper)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.usersMapper = usersMapper ?? throw new ArgumentNullException(nameof(usersMapper));
        }

        public async Task<IActionResult> AllUsers()
        {
            var currentUser = User.Identity.Name;
            var users = (await this.userService.GetAllUsersAsync()).Where(u => u.UserName != currentUser).ToList();
            var model = this.usersMapper.MapFrom(users);
            foreach (var item in model.Users)
            {
                item.Role = await this.userService.GetRoleAsync(item.UserId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateUserViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                model.AllRoles = this.userService.GetAllRoles();
                return View(model);
            }
            try
            {
                await this.userService.CreateUserAsync(model.Name, model.Email, model.Password, model.Role);
                return RedirectToAction("AllUsers", "Users");
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }

        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await this.userService.DeleteUserAsync(id);
                return Json(new { message = $"Successfully deleted user" });
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return BadRequest();
            }
        }

    }
}