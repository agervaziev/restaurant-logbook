﻿using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Controllers
{

    public class TagsController : AdminControllerBase
    {

        private readonly ITagService tagService;
        private readonly IAdminViewModelMapper<ICollection<Tag>, AllTagsViewModel> tagsMapper;

        public TagsController(ITagService tagService,
            IAdminViewModelMapper<ICollection<Tag>, AllTagsViewModel> tagsMapper)
        {
            this.tagService = tagService;
            this.tagsMapper = tagsMapper;
        }

        public async Task<IActionResult> AllTags()
        {
            var tags = await tagService.GetAllTagsAsync();
            var model = this.tagsMapper.MapFrom(tags);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AllTagsViewModel model)
        {
            var name = model.TagName;
            var tags = await tagService.GetAllTagsAsync();
            model = this.tagsMapper.MapFrom(tags);
            model.TagName = name;
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await this.tagService.CreateTagAsync(model.TagName);
                return Json(new { message = "Successfully created tag." });
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }
        }

        public async Task<IActionResult> DeleteTag(int id)
        {
            try
            {
                await this.tagService.DeleteTag(id);
                return RedirectToAction("AllTags", "Tags");
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }
        }
    }
}