﻿using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Controllers
{
    public class EstablishmentsController : AdminControllerBase
    {

        private readonly IEstablishmentService establishmentService;
        private readonly IAdminViewModelMapper<ICollection<Establishment>, AllEstablishmentsViewModel> establishmentsMapper;
        private readonly IAdminViewModelMapper<Establishment, CreateEstablishmentViewModel> establishmentMapper;
        private readonly IAdminViewModelMapper<Establishment, EstablishmentDetailsViewModel> establishmentDetailsMapper;

        public EstablishmentsController(IEstablishmentService establishmentService,
                                        IAdminViewModelMapper<ICollection<Establishment>, AllEstablishmentsViewModel> establishmentsMapper,
                                        IAdminViewModelMapper<Establishment, CreateEstablishmentViewModel> establishmentMapper,
                                        IAdminViewModelMapper<Establishment, EstablishmentDetailsViewModel> establishmentDetailsMapper)
        {
            this.establishmentService = establishmentService ?? throw new ArgumentNullException(nameof(establishmentService));
            this.establishmentsMapper = establishmentsMapper ?? throw new ArgumentNullException(nameof(establishmentsMapper));
            this.establishmentMapper = establishmentMapper ?? throw new ArgumentNullException(nameof(establishmentMapper));
            this.establishmentDetailsMapper = establishmentDetailsMapper ?? throw new ArgumentNullException(nameof(establishmentDetailsMapper));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateEstablishmentViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            try
            {

                using (var stream = new MemoryStream())
                {
                    await model.Image.CopyToAsync(stream);
                    var image = stream.ToArray();
                    await this.establishmentService.AddEstablishmentAsync(model.EstablishmentName, model.EstablishmentAddress, image, model.Details);
                    return Json(new { message = $"Successfully created {model.EstablishmentName}" });
                }
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = "Error adding establishment. Please try again later." });
            }
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var establishment = await this.establishmentService.GetEstablishmentByIdAsync(id);
            var model = this.establishmentDetailsMapper.MapFrom(establishment);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Details(EstablishmentDetailsViewModel model, int id)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            using (var stream = new MemoryStream())
            {
                if (model.Image != null)
                {
                    await model.Image.CopyToAsync(stream);
                    var image = stream.ToArray();
                    await this.establishmentService.EditEstablishmentAsync(id, model.EstablishmentName, model.EstablishmentAddress, image, model.Details);
                }
                else
                {
                    await this.establishmentService.EditEstablishmentAsync(id, model.EstablishmentName, model.EstablishmentAddress, null, model.Details);
                }
            }
            return RedirectToAction("AllEstablishments", "Establishments");
        }


        public async Task<IActionResult> AllEstablishments()
        {
            var establishments = await this.establishmentService.GetAllEstablishmentsAsync();
            var model = establishmentsMapper.MapFrom(establishments);
            return View(model);
        }

        public async Task<IActionResult> DeleteEstablishment(int id)
        {
            try
            {
                await this.establishmentService.DeleteEstablishmentAsync(id);
                return Json(new { message = $"Successfully deleted " });
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }

        }

    }
}