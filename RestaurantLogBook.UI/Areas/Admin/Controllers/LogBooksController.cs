﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RestaurantLogBook.ASPClient.Areas.Admin.Mappers.Contracts;
using RestaurantLogBook.ASPClient.Areas.Admin.Models;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Areas.Admin.Controllers
{
    public class LogBooksController : AdminControllerBase
    {
        private readonly ITagService tagService;
        private readonly IEstablishmentService establishmentService;
        private readonly ILogBookService logBookService;
        private readonly IAdminViewModelMapper<ICollection<LogBook>, AllLogBooksViewModel> logBookMapper;
        private readonly UserManager<ApplicationUser> userManager;

        public LogBooksController(ITagService tagService,
                                  IEstablishmentService establishmentService,
                                  ILogBookService logBookService,
                                  IAdminViewModelMapper<ICollection<LogBook>, AllLogBooksViewModel> logBookMapper,
                                  UserManager<ApplicationUser> userManager)
        {
            this.tagService = tagService ?? throw new ArgumentNullException(nameof(tagService));
            this.establishmentService = establishmentService ?? throw new ArgumentNullException(nameof(establishmentService));
            this.logBookService = logBookService ?? throw new ArgumentNullException(nameof(logBookService));
            this.logBookMapper = logBookMapper ?? throw new ArgumentNullException(nameof(logBookMapper));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<IActionResult> AllLogBooks()
        {
            var logBooks = await this.logBookService.GetAllLogBooksAsync();
            foreach (var lb in logBooks)
            {
                lb.Establishment = await this.logBookService.SetEstablishment(lb);
            }
            var model = this.logBookMapper.MapFrom(logBooks);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var logBook = await this.logBookService.GetLogBookBy(id);
            var viewModel = new CreateLogBookViewModel()
            {
                LogBookId = id,
                LogBookName = logBook.Name,
                SelectedEstablishment = logBook.Establishment.Name,
                SelectedTags = logBook.LogBookTags.Select(lb => lb.Tag.Name).ToList(),
                SelectedManagers = logBook.LogBookManagers.Select(lb => lb.Manager.Name).ToList(),
                AllManagerNames = (await userManager.GetUsersInRoleAsync("Manager")).Select(x => x.Name).ToList(),
                AllEstablishments = await establishmentService.GetEstablishmentNames(),
                AllTags = (await this.tagService.GetAllTagsAsync()).Select(t => t.Name).ToList()
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Details(CreateLogBookViewModel model, int id)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            await this.logBookService.EditLogBookAsync(id, model.SelectedEstablishment, model.LogBookName, model.SelectedManagers, model.SelectedTags);
            return RedirectToAction("AllLogBooks", "LogBooks");
        }

        public IActionResult Delete(int id)
        {
            try
            {
                this.logBookService.DeleteLogBook(id);
                return Json(new { message = $"Successfully deleted " });
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = "Error deleting logbook. Please try again later." });
            }
        }

        public async Task<IActionResult> Create()
        {
            var model = new CreateLogBookViewModel()
            {
                AllManagerNames = (await userManager.GetUsersInRoleAsync("Manager")).Select(u => u.Name).ToList(),
                AllEstablishments = await establishmentService.GetEstablishmentNames(),
                AllTags = (await this.tagService.GetAllTagsAsync()).Select(t => t.Name).ToList()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateLogBookViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await this.logBookService.AddLogBookAsync(model.SelectedEstablishment, model.LogBookName, model.SelectedManagers, model.SelectedTags);
                return Json(new { message = $"Successfully created {model.LogBookName}" });
            }
            catch (CustomException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return Json(new { message = "Error adding logbook. Please try again later." });
            }
        }
    }
}