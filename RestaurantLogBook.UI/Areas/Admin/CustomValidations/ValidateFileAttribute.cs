﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RestaurantLogBook.ASPClient.Areas.Admin.CustomValidations
{
    public class ValidateFileAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            var file = value as IFormFile;
            var supportedTypes = new[] { "png", "jpeg", "jpg" };
            var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
            if (!supportedTypes.Contains(fileExt))
            {
                return false;
            }
            else if (file.Length > (1000 * 1024))
            {
                return false;
            }
            return true;
        }
    }
}
