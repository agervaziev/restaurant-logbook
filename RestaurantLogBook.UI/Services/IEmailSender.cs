﻿using System.Threading.Tasks;

namespace RestaurantLogBook.ASPClient.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
