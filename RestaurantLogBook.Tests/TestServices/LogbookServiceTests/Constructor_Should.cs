﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantLogBook.Data;
using RestaurantLogBook.Services;
using System;

namespace RestaurantLogBook.Tests.TestServices.LogbookServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Create_LogbookServiceInstance_WhenValidInputIsPassed()
        {
            using (var context = new ApplicationDbContext(TestUtilities.GetOptions("Create_LogbookServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange, Act
                var sut = new LogBookService(context);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(LogBookService));
            }
        }

        [TestMethod]
        public void Throw_ArgumentNullException_WhenNullArgumentIsPassed()
        {
            using (var context = new ApplicationDbContext(TestUtilities.GetOptions("Throw_ArgumentNullException_WhenNullContextIsPassed")))
            {
                //Arrange, Act, Assert
                Assert.ThrowsException<ArgumentNullException>(() => new LogBookService(null));
            }
        }
    }
}
