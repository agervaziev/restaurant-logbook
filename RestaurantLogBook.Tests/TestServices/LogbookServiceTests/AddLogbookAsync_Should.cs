﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Tests.TestServices.LogbookServiceTests
{
    [TestClass]
    public class AddLogbookAsync_Should
    {
        [TestMethod]
        public async Task AddLogbookAsync_WhenValidInputIsPassed()
        {
            var options = TestUtilities.GetOptions("AddLogbookAsync_WhenValidInputIsPassed");

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                byte[] inputImage = new byte[] { 1, 2, 3 };
                var establishment = (new Establishment
                {
                    Name = "Hilton",
                    Address = "1 Bulgaria Boulevard, Sofia",
                    Image = inputImage,
                    Details = "Modern hotel in the heart of Sofia."
                });

                await arrangeContext.Establishments.AddAsync(establishment);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var mockManagers = new List<string>();
                var mockSelectedTags = new List<string>();

                string establishmentName = "Hilton";
                string logbookName = "Hilton Test Logbook";

                // Act

                var sut = new LogBookService(assertContext);
                var result = await sut.AddLogBookAsync(establishmentName,
                                                        logbookName,
                                                        mockManagers,
                                                        mockSelectedTags);
                // Assert
                Assert.IsInstanceOfType(result, typeof(Establishment));
            }
        }
    }
}
