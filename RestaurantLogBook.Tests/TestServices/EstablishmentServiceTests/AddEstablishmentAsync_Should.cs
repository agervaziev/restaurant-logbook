﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantLogBook.Tests.TestServices.EstablishmentServiceTests
{
    [TestClass]
    public class AddEstablishmentAsync_Should
    {
        [TestMethod]
        public async Task Return_Establishment_WhenValidInputIsPassed()
        {
            // Arrange

            byte[] inputImage = new byte[] { 1, 2, 3 };
            var establishment = (new Establishment
            {
                Name = "Hilton",
                Address = "1 Bulgaria Boulevard, Sofia",
                Image = inputImage,
                Details = "Modern hotel in the heart of Sofia."
            });

            using (var arrangeContext = new ApplicationDbContext(TestUtilities.GetOptions("Return_Establishment_WhenValidInputIsPassed")))
            {             
                await arrangeContext.Establishments.AddAsync(establishment);
                await arrangeContext.SaveChangesAsync();
            }
            
            // Assert
            Assert.IsInstanceOfType(establishment, typeof(Establishment));
            Assert.AreEqual(establishment.Name, "Hilton");
            Assert.AreEqual(establishment.Address, "1 Bulgaria Boulevard, Sofia");
            Assert.AreEqual(establishment.Details, "Modern hotel in the heart of Sofia.");            
        }

        [TestMethod]
        public async Task Throws_CustomException_WhenEstablishmentAlreadyExists()
        {
            // Arrange, Act

            byte[] inputImage = new byte[] { 1, 2, 3 };
            var establishment = (new Establishment
            {
                Name = "Hilton",
                Address = "1 Bulgaria Boulevard, Sofia",
                Image = inputImage,
                Details = "Modern hotel in the heart of Sofia."
            });

            using (var arrangeContext = new ApplicationDbContext(TestUtilities.GetOptions("Throws_CustomException_WhenEstablishmentAlreadyExists")))
            {
                await arrangeContext.Establishments.AddAsync(establishment);
                await arrangeContext.SaveChangesAsync();
            }

            // Assert

            using (var assertContext = new ApplicationDbContext(TestUtilities.GetOptions("Throws_CustomException_WhenEstablishmentAlreadyExists")))
            {
                var sut = new EstablishmentService(assertContext);
                await Assert.ThrowsExceptionAsync<CustomException>
                    (async () => await sut.AddEstablishmentAsync(
                        "Hilton",
                        "1 Bulgaria Boulevard, Sofia",
                        inputImage,
                        "Modern hotel in the heart of Sofia."));
            }
        }
    }
}
