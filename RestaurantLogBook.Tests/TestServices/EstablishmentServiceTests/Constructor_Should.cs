﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantLogBook.Data;
using RestaurantLogBook.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantLogBook.Tests.TestServices.EstablishmentServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Create_EstablishmentServiceInstance_WhenValidInputIsPassed()
        {
            using (var context = new ApplicationDbContext(TestUtilities.GetOptions("Create_EstablishmentServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange, Act
                var sut = new EstablishmentService(context);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(EstablishmentService));
            }
        }

        [TestMethod]
        public void Throw_ArgumentNullException_WhenNullArgumentIsPassed()
        {
            using (var context = new ApplicationDbContext(TestUtilities.GetOptions("Throw_ArgumentNullException_WhenNullContextIsPassed")))
            {
                //Arrange, Act, Assert
                Assert.ThrowsException<ArgumentNullException>(() => new EstablishmentService(null));
            }
        }
    }
}
