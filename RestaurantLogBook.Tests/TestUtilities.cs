﻿using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantLogBook.Tests
{
    public static class TestUtilities
    {
        public static DbContextOptions<ApplicationDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName).Options;
        }
    }
}
