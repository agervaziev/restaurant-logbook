﻿using System.Net;

namespace RestaurantLogBook.Services.Exceptions
{
    public class CustomException : BaseDbException
    {
        public CustomException(string message)
           : base(message, (int)HttpStatusCode.Created)
        {
        }

    }
}
