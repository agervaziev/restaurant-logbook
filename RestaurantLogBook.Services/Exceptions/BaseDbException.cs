﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantLogBook.Services.Exceptions
{
    public class BaseDbException : Exception
    {
        private readonly int code;

        public int Code { get; set; }

        // constructor
        public BaseDbException(string message, int code)
            : base(message)
        {
            this.code = code;
        }
    }
}
