﻿using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services
{
    public class LogBookService : ILogBookService
    {
        private readonly ApplicationDbContext context;

        public LogBookService(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }


        public void DeleteLogBook(int id)
        {
            var logbook = this.context.LogBooks.Find(id);
            if (logbook == null || logbook.IsDeleted == true)
            {
                throw new CustomException
                   ($"LogBook is not in the database");
            }
            logbook.IsDeleted = true;
            if (logbook.LogBookManagers != null)
            {
                this.context.RemoveRange(logbook.LogBookManagers);
            }
            if (logbook.LogBookTags != null)
            {
                this.context.RemoveRange(logbook.LogBookTags);
            }
            this.context.Update(logbook);
            this.context.SaveChanges();
        }

        public async Task<LogBook> GetLogBookBy(int id)
        {
            if (await this.context.LogBooks.FindAsync(id) == null || (await this.context.LogBooks.FindAsync(id)).IsDeleted == true)
            {
                throw new CustomException
                   ($"LogBook is not in the database");
            }
            return await this.context.LogBooks
                .Include(lb => lb.Establishment)
                .Include(lb => lb.LogBookManagers)
                    .ThenInclude(lbm => lbm.Manager)
                .Include(lb => lb.LogBookTags)
                    .ThenInclude(lbt => lbt.Tag)
                 .Include(lb => lb.ManagerTasks)
                    .ThenInclude(mc => mc.ManagerCommentTags)
                        .ThenInclude(mct => mct.Tag)
                .FirstOrDefaultAsync(t => t.LogBookId == id);
        }

        public string GetEstablishmentName(LogBook logBook)
        {
            if (this.context.LogBooks.Find(logBook.LogBookId) == null || this.context.LogBooks.Find(logBook.LogBookId).IsDeleted == true)
            {
                throw new CustomException
             ($"LogBook is not in the database");
            }
            return this.context.Establishments.Find(logBook.EstablishmentId).Name;
        }

        public async Task<ICollection<LogBook>> GetAllLogBooksAsync()
        {
            return await this.context.LogBooks.Where(lb => lb.IsDeleted == false)
                .Include(l => l.LogBookManagers)
                    .ThenInclude(lb => lb.Manager)
                .Include(l => l.ManagerTasks)
                .Include(l => l.LogBookTags)
                    .ThenInclude(lbt => lbt.Tag)
                .ToListAsync();
        }

        public async Task<Establishment> SetEstablishment(LogBook logBook)
        {
            if (this.context.LogBooks.Find(logBook.LogBookId) == null || logBook.IsDeleted == true)
            {
                throw new CustomException
             ($"LogBook is not in the database");
            }
            return await this.context.Establishments.FindAsync(logBook.EstablishmentId);
        }

        public async Task<ICollection<LogBook>> GetAllLogBooksAsync(string userName)
        {
            var managerId = this.context.Users.FirstOrDefault(u => u.UserName == userName).Id;
            var logBooks = this.context.LogBooks.Where(lb => lb.IsDeleted == false)
                .Include(l => l.LogBookManagers)
                    .ThenInclude(lb => lb.Manager)
                .Include(l => l.ManagerTasks)
                .Include(l => l.LogBookTags)
                    .ThenInclude(lbt => lbt.Tag);

            return await logBooks.Where(lb => lb.LogBookManagers.Any(lbm => lbm.ManagerId == managerId)).ToListAsync();

        }

        public async Task<Establishment> AddLogBookAsync(string establishmentName, string logBookName, ICollection<string> managerNames, ICollection<string> selectedTags)
        {
            var establishment = await this.context.Establishments
                .Include(e => e.LogBooks)
                .SingleOrDefaultAsync(e => e.Name == establishmentName);
            if (establishment.LogBooks.Any(lb => lb.Name == logBookName) && establishment.LogBooks.FirstOrDefault(lb => lb.Name == logBookName).IsDeleted == false)
            {
                throw new CustomException
                    ($"LogBook with this name exists for this establishment. ");
            }

            if (establishment.LogBooks.Count == 0)
            {
                establishment.LogBooks = new List<LogBook>();
            }
            if (managerNames == null)
            {
                managerNames = new List<string>();
            }
            var managers = new List<ApplicationUser>();

            foreach (var manager in managerNames)
            {
                managers.Add(context.Users.SingleOrDefault(u => u.Name == manager));
            }

            if (selectedTags == null)
            {
                selectedTags = new List<string>();
            }
            var tags = new List<Tag>();
            foreach (var tag in selectedTags)
            {
                tags.Add(context.Tags.SingleOrDefault(t => t.Name == tag));
            }


            var newLogBook = new LogBook()
            {
                Name = logBookName,
                ManagerTasks = new List<ManagerTask>(),
                EstablishmentId = establishment.EstablishmentId
            };

            newLogBook.LogBookManagers = managers
                .Select(manager => new LogBookManagers() { Manager = manager })
              .ToList();
            newLogBook.LogBookTags = tags
                .Select(tag => new LogBookTags() { Tag = tag })
              .ToList();

            this.context.Update(establishment);
            await this.context.LogBooks.AddAsync(newLogBook);
            await this.context.SaveChangesAsync();
            return establishment;
        }

        public async Task<LogBook> EditLogBookAsync(int id, string establishmentName, string logBookName, ICollection<string> managerNames, ICollection<string> selectedTags)
        {
            if (await this.context.LogBooks.FindAsync(id) == null || (await this.context.LogBooks.FindAsync(id)).IsDeleted == true)
            {
                throw new CustomException
                   ($"LogBook is not in the database");
            }
            if (this.context.Establishments.FirstOrDefaultAsync(e => e.Name == establishmentName) == null)
            {
                throw new CustomException
                   ($"Establishment is not in the database");
            }
            var logBook = await this.context.LogBooks
                 .Include(lb => lb.Establishment)
                 .Include(lb => lb.LogBookManagers)
                     .ThenInclude(lbm => lbm.Manager)
                 .Include(lb => lb.LogBookTags)
                     .ThenInclude(lbt => lbt.Tag)
                  .Include(lb => lb.ManagerTasks)
                 .FirstOrDefaultAsync(t => t.LogBookId == id);

            var establishment = await this.context.Establishments
               .Include(e => e.LogBooks)
               .SingleOrDefaultAsync(e => e.Name == establishmentName);

            if (managerNames == null)
            {
                managerNames = new List<string>();
            }
            var managers = new List<ApplicationUser>();

            foreach (var manager in managerNames)
            {
                managers.Add(context.Users.SingleOrDefault(u => u.Name == manager));
            }
            if (selectedTags == null)
            {
                selectedTags = new List<string>();
            }

            var tags = new List<Tag>();
            foreach (var tag in selectedTags)
            {
                tags.Add(context.Tags.SingleOrDefault(t => t.Name == tag));
            }
            logBook.Name = logBookName;
            logBook.Establishment = establishment;
            logBook.EstablishmentId = establishment.EstablishmentId;

            logBook.LogBookManagers = managers
                .Select(manager => new LogBookManagers() { Manager = manager })
              .ToList();
            logBook.LogBookTags = tags
                 .Select(tag => new LogBookTags() { Tag = tag })
               .ToList();
            this.context.Update(logBook);
            await this.context.SaveChangesAsync();
            return logBook;
        }
    }
}
