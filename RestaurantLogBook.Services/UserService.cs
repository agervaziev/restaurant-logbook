﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public UserService(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }


        public ICollection<string> GetAllRoles()
        {
            return this.context.Roles.Select(x => x.Name).ToList();
        }

        public async Task DeleteUserAsync(string id)
        {
            var user = await this.userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new Exception
                   ($"User is not in the database");
            }
            if (user.Name == "Admin")
            {
                throw new Exception
                  ($"Admin cannot be removed");
            }
            //this.context.Users.Remove(user);

            //Refactor to work with IsDeleted property
            this.context.Users.Remove(user);
            await this.context.SaveChangesAsync();
        }

        public async Task<string> GetRoleAsync(string id)
        {
            if (await this.context.Users.FindAsync(id) == null)
            {
                throw new Exception
                    ($"User is not in the database");
            }
            var roleId = (await this.context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == id)).RoleId;
            return this.context.Roles.Find(roleId).Name;
        }

        public async Task<ICollection<ApplicationUser>> GetAllUsersAsync()
        {
            return await this.context.Users.Where(u => u.Name != "Admin")
                .Include(u => u.LogBookManagers)
                    .ThenInclude(lbm => lbm.Manager)
                .ToListAsync();
        }

        public async Task<ApplicationUser> CreateUserAsync(string name, string email, string password, string role)
        {
            if (await this.context.Users.AnyAsync(u => u.Name == name))
            {
                throw new Exception
                   ($"Username already exists in the database");
            }
            var user = new ApplicationUser { Name = name, Email = email, UserName = name };
            user.LogBookManagers = new List<LogBookManagers>();
            var result = await userManager.CreateAsync(user, password);
            await userManager.AddToRoleAsync(user, role);
            return user;
        }
    }
}
