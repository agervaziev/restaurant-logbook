﻿using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services
{
    public class EstablishmentService : IEstablishmentService
    {
        private readonly ApplicationDbContext context;

        public EstablishmentService(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }


        public async Task DeleteEstablishmentAsync(int id)
        {
            var establishment = await this.context.Establishments
                .FirstOrDefaultAsync(e => e.EstablishmentId == id && e.IsDeleted == false);

            if (establishment == null)
            {
                throw new CustomException
                    ($"The establishment you are trying to delete does not exist. ");
            }

            establishment.IsDeleted = true;
            if (establishment.LogBooks != null)
            {
            this.context.RemoveRange(establishment.LogBooks);
            }
            this.context.Update(establishment);
            await this.context.SaveChangesAsync();
        }

        public async Task<Establishment> GetEstablishmentByIdAsync(int id)
        {
            var establishment = await this.context.Establishments
                .Include(e => e.CustomerComments)
                .Include(e => e.LogBooks)
                .FirstOrDefaultAsync(e => e.EstablishmentId == id && e.IsDeleted == false);

            if (establishment == null || establishment.IsDeleted == true)
            {
                throw new CustomException
                    ($"The establishment you are trying to access does not exist. ");
            }

            return establishment;
        }

        public async Task<ICollection<string>> GetEstablishmentNames()
        {
            return await this.context.Establishments
                .Where(e => e.IsDeleted == false)
                .Select(e => e.Name).ToListAsync();
        }

        public async Task<ICollection<Establishment>> GetAllEstablishmentsAsync()
        {
            var establishments = await this.context.Establishments.Where(e => e.IsDeleted == false)
                .Include(e => e.LogBooks)
                .Select(x => new Establishment() { EstablishmentId = x.EstablishmentId, Name = x.Name, LogBooks = x.LogBooks.Where(l => l.IsDeleted == false).ToList() })
                .ToListAsync();

            return establishments;
        }

        public async Task<ICollection<Establishment>> SearchEstablishment(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return await this.context.Establishments.Where(e => e.IsDeleted == false)
                .Include(e => e.CustomerComments).ToListAsync();
            }
            var establishments = await this.context.Establishments.Where(e => e.IsDeleted == false)
                .Include(e => e.CustomerComments)
                .Where(e => e.Name.ToLower().Contains(input.ToLower())).ToListAsync();
            return establishments;
        }

        public async Task<ICollection<Establishment>> GetAllEstablishmentsAsync(bool skip, int number)
        {
            if (!skip)
            {
                return await this.context.Establishments.Where(e => e.IsDeleted == false).Take(number)
                                .Include(e => e.CustomerComments)
                                .ToListAsync();
            }
            return await this.context.Establishments.Where(e => e.IsDeleted == false).Skip(number)
                                .Include(e => e.CustomerComments)
                                .ToListAsync();
        }

        public async Task<IReadOnlyCollection<Establishment>> ShowEstablishmentsByNameAsync(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return await this.context.Establishments.Where(e => e.IsDeleted == false)
                .Include(e => e.CustomerComments).ToListAsync();
            }
            var establishments = await this.context.Establishments.Where(e => e.IsDeleted == false)
                .Include(e => e.CustomerComments)
                .Where(e => e.Name.ToLower().Contains(input.ToLower())).ToListAsync();
            return establishments;
        }

        public async Task<Establishment> AddEstablishmentAsync(string name, string address, byte[] image, string details)
        {

            if (await this.context.Establishments.AnyAsync
                (e => e.Name == name && e.IsDeleted == false))
            {
                throw new CustomException
                    ($"Establishment with this name already exists in the database. ");
            }

            if (await this.context.Establishments.AnyAsync(e => e.Name == name && e.IsDeleted == true))
            {
                var est = await this.context.Establishments.FirstAsync(e => e.Name == name);
                est.IsDeleted = false;
                est.Name = name;
                est.Address = address;
                est.Details = details;
                est.Image = image;
                est.CustomerComments = new List<CustomerComment>();
                est.LogBooks = new List<LogBook>() { };
                this.context.Update(est);
                await this.context.SaveChangesAsync();
                return est;
            }

            Establishment establishment = new Establishment()
            {
                Name = name,
                Address = address,
                Image = image,
                Details = details,
                CustomerComments = new List<CustomerComment>(),
                LogBooks = new List<LogBook>() { }

            };
            await this.context.Establishments.AddAsync(establishment);
            await this.context.SaveChangesAsync();

            return establishment;
        }

        public async Task<Establishment> EditEstablishmentAsync(int id, string name, string address, byte[] image, string details)
        {
            var establishment = await this.context.Establishments.FindAsync(id);
            if (establishment == null || establishment.IsDeleted == true)
            {
                throw new CustomException
                   ($"The establishment you are trying to edit does not exist. ");
            }
            establishment.Name = name;
            establishment.Address = address;
            if (image != null)
            {
                establishment.Image = image;
            }
            establishment.Details = details;
            this.context.Update(establishment);
            await this.context.SaveChangesAsync();
            return establishment;
        }
    }
}
