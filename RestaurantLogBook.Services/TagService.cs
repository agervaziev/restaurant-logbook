﻿using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services
{
    public class TagService : ITagService
    {
        private readonly ApplicationDbContext context;

        public TagService(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }


        public async Task DeleteTag(int id)
        {
            var tag = await this.context.Tags.SingleOrDefaultAsync(t => t.TagId == id);
            if (tag == null)
            {
                throw new CustomException("Tag does not exist in the database");
            }
            this.context.Tags.Remove(tag);
            await this.context.SaveChangesAsync();
        }

        public async Task<Tag> CreateTagAsync(string name)
        {
            if (await this.context.Tags.AnyAsync(e => e.Name == name))
            {
                throw new CustomException
                    ($"Tag with this name already exists in the database. ");
            }
            Tag tag = new Tag()
            {
                Name = name,
            };
            await this.context.Tags.AddAsync(tag);

            await this.context.SaveChangesAsync();

            return tag;
        }

        public async Task<ICollection<Tag>> GetAllTagsAsync()
        {
            return await this.context.Tags
                .Include(t => t.ManagerCommentTags)
                    .ThenInclude(mt => mt.ManagerComment)
                .ToListAsync();
        }
    }
}
