﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RestaurantLogBook.Services
{
    public static class HelperMethods
    {
        public static string FilterSwearWords(string text)
        {
            var newText = text.ToLower();
            using (StreamReader r = new StreamReader("wwwroot/swearWords.json"))
            {
                string json = r.ReadToEnd();
                List<string> allSwearWords = JsonConvert.DeserializeObject<List<string>>(json);
                if (allSwearWords.Any(s => newText.Contains(s.ToLower())))
                {
                    var matchList = allSwearWords.Where(s => text.Contains(s));
                    foreach (var item in matchList)
                    {
                        newText = newText.Replace(item, "****");
                    }
                }
            }
            return newText;
        }
    }
}
