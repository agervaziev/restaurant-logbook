﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services.Contracts
{
    public interface IManagerTaskService
    {
        Task<ManagerTask> ChangeTaskStatusAsync(int id);

        Task<ManagerTask> AddManagerTaskAsync(string author, int logBookId, ICollection<string> selectedTags, string text, string assignee);
    }
}
