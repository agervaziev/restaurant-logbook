﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services.Contracts
{
    public interface IUserService
    {
        Task DeleteUserAsync(string id);

        ICollection<string> GetAllRoles();

        Task<string> GetRoleAsync(string id);

        Task<ICollection<ApplicationUser>> GetAllUsersAsync();

        Task<ApplicationUser> CreateUserAsync(string name, string email, string password, string role);
    }
}
