﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services.Contracts
{
    public interface ICustommerCommentService
    {
        ICollection<CustomerComment> GetAll(int establishmentId);

        Task<CustomerComment> AddCustomerCommentAsync(int establishmentId, string authorName, string text, int rating);
    }
}
