﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services.Contracts
{
    public interface ILogBookService
    {
        void DeleteLogBook(int id);

        Task<LogBook> GetLogBookBy(int id);

        string GetEstablishmentName(LogBook logBook);

        Task<ICollection<LogBook>> GetAllLogBooksAsync();

        Task<Establishment> SetEstablishment(LogBook logBook);

        Task<ICollection<LogBook>> GetAllLogBooksAsync(string userName);

        Task<Establishment> AddLogBookAsync(string establishment, string logBookName, ICollection<string> managerNames, ICollection<string> selectedTags);

        Task<LogBook> EditLogBookAsync(int id, string establishmentName, string logBookName, ICollection<string> managerNames, ICollection<string> selectedTags);
    }
}
