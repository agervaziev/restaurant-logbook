﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services.Contracts
{
    public interface ITagService
    {
        Task DeleteTag(int id);

        Task<Tag> CreateTagAsync(string name);

        Task<ICollection<Tag>> GetAllTagsAsync();
    }
}
