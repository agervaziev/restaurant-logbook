﻿using RestaurantLogBook.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services.Contracts
{
    public interface IEstablishmentService
    {
        Task DeleteEstablishmentAsync(int id);

        Task<ICollection<string>> GetEstablishmentNames();

        Task<Establishment> GetEstablishmentByIdAsync(int id);

        Task<ICollection<Establishment>> GetAllEstablishmentsAsync();

        Task<ICollection<Establishment>> SearchEstablishment(string input);

        Task<ICollection<Establishment>> GetAllEstablishmentsAsync(bool skip, int number);

        Task<Establishment> AddEstablishmentAsync(string name, string address, byte[] image, string details);

        Task<Establishment> EditEstablishmentAsync(int id, string name, string address, byte[] image, string details);
    }
}
