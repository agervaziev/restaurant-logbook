﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services
{
    public class ManagerTaskService : IManagerTaskService
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;

        public ManagerTaskService(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }


        public async Task<ManagerTask> ChangeTaskStatusAsync(int id)
        {
            var task = await this.context.ManagerComments.FindAsync(id);
            if (task == null)
            {
                throw new CustomException("Task is not in the database");
            }
            task.IsDone = !task.IsDone;
            this.context.Update(task);
            await this.context.SaveChangesAsync();
            return task;
        }

        public async Task<ManagerTask> AddManagerTaskAsync(string author, int logBookId, ICollection<string> selectedTags, string text, string assigneeName)
        {
            var logBook = await this.context.LogBooks.FindAsync(logBookId);
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == author);
            if (selectedTags == null || selectedTags.Count == 0 || selectedTags.First() == null)
            {
                selectedTags = new List<string>();
            }
            var tags = new List<Tag>();
            foreach (var tag in selectedTags)
            {
                tags.Add(await context.Tags.SingleOrDefaultAsync(t => t.Name == tag));
            }
            var assignee = await this.context.Users.SingleOrDefaultAsync(t => t.Name == assigneeName);
            var task = new ManagerTask()
            {
                Author = user,
                Date = DateTime.Now,
                LogBookId = logBookId,
                Text = text,
                Assignee = assignee
            };

            task.ManagerCommentTags = tags
                .Select(tag => new ManagerTaskTags() { Tag = tag })
              .ToList();
            logBook.ManagerTasks.Add(task);
            this.context.Update(logBook);
            await this.context.ManagerComments.AddAsync(task);
            await this.context.SaveChangesAsync();
            return task;
        }


    }
}