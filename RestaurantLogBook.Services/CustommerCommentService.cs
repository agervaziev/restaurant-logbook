﻿using RestaurantLogBook.Data;
using RestaurantLogBook.Data.Models;
using RestaurantLogBook.Services.Contracts;
using RestaurantLogBook.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantLogBook.Services
{
    public class CustommerCommentService : ICustommerCommentService
    {
        private readonly ApplicationDbContext context;

        public CustommerCommentService(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }


        public ICollection<CustomerComment> GetAll(int establishmentId)
        {
            if (!this.context.Establishments.Any(x => x.EstablishmentId == establishmentId))
            {
                throw new CustomException("Establishment not found.");
            }
            return this.context.CustomerComments.OrderByDescending(cc => cc.Date).ToList();
        }

        public async Task<CustomerComment> AddCustomerCommentAsync(int establishmentId, string authorName, string text, int rating)
        {

            if (!this.context.Establishments.Any(x => x.EstablishmentId == establishmentId))
            {
                throw new CustomException("Establishment not found.");
            }
            var establishment = await this.context.Establishments.FindAsync(establishmentId);

            var newtext = HelperMethods.FilterSwearWords(text);
            var newAuthor = "";
            if (!string.IsNullOrEmpty(authorName))
            {
                newAuthor = HelperMethods.FilterSwearWords(authorName);
            }
            var comment = new CustomerComment()
            {
                Author = newAuthor,
                Date = DateTime.Now,
                EstablishmentId = establishmentId,
                Establishment = await this.context.Establishments.FindAsync(establishmentId),
                Rating = rating,
                Text = newtext
            };
            if (!string.Equals(text, newtext) || !string.Equals(authorName, newAuthor))
            {
                comment.IsFlagged = true;
            }
            if (establishment.CustomerComments == null)
            {
                establishment.CustomerComments = new List<CustomerComment>() { comment };
            }
            else
            {
                establishment.CustomerComments.Add(comment);
            }
            this.context.Update(establishment);
            await this.context.AddAsync(comment);
            await this.context.SaveChangesAsync();
            return comment;
        }

    }
}
