﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RestaurantLogBook.Data.Models;

namespace RestaurantLogBook.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public ApplicationDbContext() { }

        public DbSet<Tag> Tags { get; set; }
        public DbSet<LogBook> LogBooks { get; set; }
        public DbSet<ManagerTask> ManagerComments { get; set; }
        public DbSet<Establishment> Establishments { get; set; }
        public DbSet<CustomerComment> CustomerComments { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ManagerTaskTags>()
                .HasKey(mt => new { mt.ManagerCommentId, mt.TagId });

            modelBuilder.Entity<LogBookManagers>()
                .HasKey(em => new { em.LogBookId, em.ManagerId });

            modelBuilder.Entity<LogBookTags>()
                .HasKey(lt => new { lt.LogBookId, lt.TagId });

            modelBuilder.Entity<ManagerTask>()
            .HasOne<ApplicationUser>(m => m.Assignee)
            .WithMany(u => u.AssignedManagerTasks)
            .HasForeignKey(s => s.AssigneeId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
