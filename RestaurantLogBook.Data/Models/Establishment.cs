﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.Data.Models
{
    public class Establishment 
    {
        [Key]
        public int EstablishmentId { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Name must be between 3 and 30 symbols", MinimumLength = 3)]
        public string Name { get; set; }

        public byte[] Image { get; set; }
        public bool IsDeleted { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Address must be between 10 and 50 symbols", MinimumLength = 10)]
        public string Address { get; set; }

        [StringLength(1000, ErrorMessage = "Address must be between 5 and 1000 symbols", MinimumLength = 5)]
        public string Details { get; set; }

        public ICollection<LogBook> LogBooks { get; set; }
        public ICollection<CustomerComment> CustomerComments { get; set; }

    }
}
