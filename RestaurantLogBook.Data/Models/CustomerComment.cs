﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.Data.Models
{
    public class CustomerComment
    {
        public int CustomerCommentId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string Text { get; set; }

        public string Author { get; set; }
        public bool IsDone { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsFlagged { get; set; }

        [Range(0, 5)]
        public int Rating { get; set; }

        [Required]
        public int EstablishmentId { get; set; }

        [Required]
        public Establishment Establishment { get; set; }

    }
}
