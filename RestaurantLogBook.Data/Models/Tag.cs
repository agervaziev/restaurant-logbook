﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.Data.Models
{
    public class Tag
    {
        [Key]
        public int TagId { get; set; }

        [Required]
        [MaxLength(15)]
        [MinLength(3)]
        public string Name { get; set; }

        public ICollection<LogBookTags> LogBookTags { get; set; }
        public ICollection<ManagerTaskTags> ManagerCommentTags { get; set; }

    }
}
