﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.Data.Models
{
    public class LogBook
    {
        [Key]
        public int LogBookId { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Name must be between 3 and 30 symbols", MinimumLength = 3)]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        public int EstablishmentId { get; set; }

        public ICollection<ManagerTask> ManagerTasks { get; set; }
        public ICollection<LogBookTags> LogBookTags { get; set; }
        public ICollection<LogBookManagers> LogBookManagers { get; set; }
        public Establishment Establishment { get; set; }
    }
}
