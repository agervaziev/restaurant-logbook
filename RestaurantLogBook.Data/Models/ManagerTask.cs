﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantLogBook.Data.Models
{
    public class ManagerTask
    {
        [Key]
        public int ManagerCommentId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Text must be between 5 and 200 characters", MinimumLength = 5)]
        public string Text { get; set; }

        public bool IsDone { get; set; }
        public bool IsDeleted { get; set; }

        public string AssigneeId { get; set; }
        public ApplicationUser Assignee { get; set; }

        [Required]
        public string AuthorId { get; set; }
        [Required]
        public ApplicationUser Author { get; set; }

        [Required]
        public int LogBookId { get; set; }

        [Required]
        public LogBook LogBook { get; set; }

        public ICollection<ManagerTaskTags> ManagerCommentTags { get; set; }
    }
}
