﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace RestaurantLogBook.Data.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public bool IsLogged { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<ManagerTask> ManagerTasks { get; set; }
        public ICollection<ManagerTask> AssignedManagerTasks { get; set; }
        public ICollection<LogBookManagers> LogBookManagers { get; set; }
    }
}
