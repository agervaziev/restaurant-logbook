﻿namespace RestaurantLogBook.Data.Models
{
    public class ManagerTaskTags
    {
        public int ManagerCommentId { get; set; }
        public ManagerTask ManagerComment { get; set; }


        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
