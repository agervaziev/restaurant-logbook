﻿namespace RestaurantLogBook.Data.Models
{
    public class LogBookTags
    {
        public int LogBookId { get; set; }
        public LogBook LogBook { get; set; }


        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
