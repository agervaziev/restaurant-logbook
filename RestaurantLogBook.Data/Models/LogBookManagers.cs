﻿namespace RestaurantLogBook.Data.Models
{
    public class LogBookManagers
    {
        public string ManagerId { get; set; }
        public ApplicationUser Manager { get; set; }


        public int LogBookId { get; set; }
        public LogBook LogBook{ get; set; }
    }
}
