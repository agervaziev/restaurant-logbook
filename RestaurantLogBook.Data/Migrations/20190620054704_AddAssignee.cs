﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantLogBook.Data.Migrations
{
    public partial class AddAssignee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ManagerCommentTags");

            migrationBuilder.AddColumn<string>(
                name: "AssigneeId",
                table: "ManagerComments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ManagerTaskTags",
                columns: table => new
                {
                    ManagerCommentId = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManagerTaskTags", x => new { x.ManagerCommentId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ManagerTaskTags_ManagerComments_ManagerCommentId",
                        column: x => x.ManagerCommentId,
                        principalTable: "ManagerComments",
                        principalColumn: "ManagerCommentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManagerTaskTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "TagId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ManagerComments_AssigneeId",
                table: "ManagerComments",
                column: "AssigneeId");

            migrationBuilder.CreateIndex(
                name: "IX_ManagerTaskTags_TagId",
                table: "ManagerTaskTags",
                column: "TagId");

            migrationBuilder.AddForeignKey(
                name: "FK_ManagerComments_AspNetUsers_AssigneeId",
                table: "ManagerComments",
                column: "AssigneeId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ManagerComments_AspNetUsers_AssigneeId",
                table: "ManagerComments");

            migrationBuilder.DropTable(
                name: "ManagerTaskTags");

            migrationBuilder.DropIndex(
                name: "IX_ManagerComments_AssigneeId",
                table: "ManagerComments");

            migrationBuilder.DropColumn(
                name: "AssigneeId",
                table: "ManagerComments");

            migrationBuilder.CreateTable(
                name: "ManagerCommentTags",
                columns: table => new
                {
                    ManagerCommentId = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManagerCommentTags", x => new { x.ManagerCommentId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ManagerCommentTags_ManagerComments_ManagerCommentId",
                        column: x => x.ManagerCommentId,
                        principalTable: "ManagerComments",
                        principalColumn: "ManagerCommentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManagerCommentTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "TagId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ManagerCommentTags_TagId",
                table: "ManagerCommentTags",
                column: "TagId");
        }
    }
}
